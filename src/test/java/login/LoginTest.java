package login;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import static org.testng.Assert.*;
import static org.testng.Assert.assertTrue;

import pages.HomePage;
import pages.LoginPage;
import base.TestBase;
import java.io.IOException;
import java.net.MalformedURLException;

import org.testng.annotations.BeforeMethod;
import util.TestUtil;

public class LoginTest extends TestBase {

    LoginPage loginPage;
    HomePage homePage;
    String validLoginUserName;
    String validLoginPassword;
    String invalidLoginPassword;
    String invalidLoginEmailAddress;


    public LoginTest(){
        super();
    }

    @BeforeMethod
    public void setUp() throws MalformedURLException {
        String mmLoginUrl = prop.getProperty("LoginUrl");
        validLoginUserName = prop.getProperty("TestUserName");
        validLoginPassword = prop.getProperty("TestPassword");
        invalidLoginEmailAddress = prop.getProperty("TestInvalidEmailAddress");
        invalidLoginPassword = prop.getProperty("TestInvalidPassword");


        initialization(mmLoginUrl);
        loginPage = new LoginPage(driver);
    }

    @Test(priority=1)
    public void testClientLoginWithInvalidEmail() throws IOException {
        Boolean errorDisplayed = loginPage.validateInvalidEmailAddress(invalidLoginEmailAddress);

        if (errorDisplayed){
            String errorMessage = loginPage.getInvalidEmailErrorMessage();
            TestUtil.takeScreenshotAtEndOfTest(driver);

            assertTrue(true, errorMessage);
        }else{
            fail("Invalid email address was accepted");
        }
    }

    @Test(priority=2)
    public void testClientLoginWithInvalidPassword() throws IOException {
        Boolean errorDisplayed = loginPage.validateInvalidPassword(validLoginUserName, invalidLoginPassword);
        if (errorDisplayed)
        {
            String errorMessage = loginPage.getInvalidEmailErrorMessage();
            TestUtil.takeScreenshotAtEndOfTest(driver);

            assertTrue(true, errorMessage);
        }else{
            fail("Invalid password was accepted");
        }
        String errorMessage = loginPage.getInvalidPasswordErrorMessage();
        TestUtil.takeScreenshotAtEndOfTest(driver);

        assertTrue(true,
                errorMessage);
    }

    @Test(priority=3)
    public void testClientLoginRequiredUserName() throws IOException {
        Boolean errorDisplayed = loginPage.validateRequiredLoginPassword(validLoginUserName);
        if (errorDisplayed)
        {
            String errorMessage = loginPage.getInvalidEmailErrorMessage();
            TestUtil.takeScreenshotAtEndOfTest(driver);

            assertTrue(true, errorMessage);
        }else{
            fail("Invalid password was accepted");
        }
        String errorMessage = loginPage.getInvalidPasswordErrorMessage();
        TestUtil.takeScreenshotAtEndOfTest(driver);

        assertTrue(true,
                errorMessage);
    }

    @Test(priority=4)
    public void testClientLoginRequiredPassword() throws IOException {
        Boolean errorDisplayed = loginPage.validateRequiredLoginPassword(validLoginUserName);
        if (errorDisplayed)
        {
            String errorMessage = loginPage.getInvalidEmailErrorMessage();
            TestUtil.takeScreenshotAtEndOfTest(driver);

            assertTrue(true, errorMessage);
        }else{
            fail("Invalid password was accepted");
        }
        String errorMessage = loginPage.getInvalidPasswordErrorMessage();
        TestUtil.takeScreenshotAtEndOfTest(driver);

        assertTrue(true,
                errorMessage);
    }


    @Test(priority=5)
    public void testClientLoginSuccess() {
        homePage = loginPage.login(validLoginUserName, validLoginPassword);

        assertTrue(homePage.getUserAvatar(),
                "Login was not successfull");
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}
