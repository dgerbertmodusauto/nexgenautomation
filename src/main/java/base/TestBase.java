package base;

import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import util.TestUtil;
import util.WebEventListener;

public class TestBase {

    public static WebDriver driver;
    public static Properties prop;
    public  static EventFiringWebDriver e_driver;
    public static WebEventListener eventListener;
    private static URL baseUrl;
    private static HttpURLConnection connection = null;
    private static Logger LOGGER;


    public TestBase(){
        try {
            PropertyConfigurator.configure("log4j.properties");
            prop = new Properties();
            FileInputStream ip = new FileInputStream(System.getProperty("user.dir")+ "/src/main/java/config/config.properties");
            prop.load(ip);
            LOGGER = LogManager.getLogger(TestBase.class);
            LOGGER.info("TestBase completed");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void initialization(String url) throws MalformedURLException {
        String browserName = prop.getProperty("browser");
        String bsUserName = prop.getProperty("BROWSERSTACK_USERNAME");
        String bsApiKey = prop.getProperty("BROWSERSTACK_AUTOMATE_KEY");
        String bsUrl = "https://" + bsUserName + ":" + bsApiKey + "@hub-cloud.browserstack.com/wd/hub";

        //if(browserName.equals("chrome")){
            //System.setProperty("webdriver.chrome.driver", "resources/chromedriver/93/chromedriver.exe");
            //driver = new ChromeDriver();
        //}

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setPlatform(Platform.WINDOWS);
        caps.setBrowserName("chrome");
        caps.setCapability("browser_version", "latest");
        caps.setCapability("browserstack.debug", "true");
        caps.setCapability("build", "MMMAutomation-V1");
        caps.setCapability("name", "Login Test");
        caps.setCapability("browserstack.console", "info");  // to enable console logs at the info level. You can also use other log levels here
        caps.setCapability("browserstack.networkLogs", "true");  // to enable network logs to be logged
        driver = new RemoteWebDriver(new URL(bsUrl), caps);

        e_driver = new EventFiringWebDriver(driver);
        // Now create object of EventListerHandler to register it with EventFiringWebDriver
        eventListener = new WebEventListener();
        e_driver.register(eventListener);
        driver = e_driver;

        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        try {
            baseUrl = new URL(url);
            connection = (HttpURLConnection) baseUrl.openConnection();
            connection.connect();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                LOGGER.error("Unable to connect to the url connection");
                LOGGER.error("Response Code " + connection.getResponseCode());
                LOGGER.error("Response Message " + connection.getResponseMessage());
            }
            driver.get(url);
        } catch (IOException e) {
            LOGGER.error("URL connection error");
            e.printStackTrace();
        }
    }
}

