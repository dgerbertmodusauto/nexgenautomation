package pages;

import base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends TestBase {
    private WebDriver driver;
    private By usernameField = By.id("username");
    private By passwordField = By.id("password");
    private By groupModal = By.className("supernova-modal-content");
    private By groupToSelect = By.xpath("//span[contains(text(),'Content Examples')]");
    private By loginButton = By.className("btn-login");
    private By errorMessage = By.className("toast-message");
    private By errorContainer = By.className(("toast-error"));

    public LoginPage(WebDriver driver){
        this.driver = driver;
    }

    public HomePage login(String userName, String password){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginButton));
        driver.findElement(loginButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(usernameField));
        driver.findElement(usernameField).sendKeys(userName);
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginButton));
        driver.findElement(loginButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(passwordField));
        driver.findElement(passwordField).sendKeys(password);
        driver.findElement(loginButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(groupModal));
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(groupToSelect));
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);

        return new HomePage(driver);
    }

    public Boolean validateInvalidEmailAddress(String userName){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginButton));
        driver.findElement(loginButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(usernameField));
        driver.findElement(usernameField).sendKeys(userName);
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginButton));
        driver.findElement(loginButton).click();

        WebElement errorMessageElement = wait.until(ExpectedConditions.visibilityOfElementLocated(errorMessage));

        return errorMessageElement.isDisplayed();
    }

    public Boolean validateInvalidPassword(String userName, String password){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginButton));
        driver.findElement(loginButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(usernameField));
        driver.findElement(usernameField).sendKeys(userName);
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginButton));
        driver.findElement(loginButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(passwordField));
        driver.findElement(passwordField).sendKeys(password);
        driver.findElement(loginButton).click();

        WebElement errorMessageElement = wait.until(ExpectedConditions.visibilityOfElementLocated(errorMessage));

        return errorMessageElement.isDisplayed();
    }

    public Boolean validateRequiredLoginUserName(String userName){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginButton));
        driver.findElement(loginButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(usernameField));
        driver.findElement(usernameField).sendKeys(userName);
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginButton));
        driver.findElement(loginButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(passwordField));
        driver.findElement(usernameField).sendKeys("");
        driver.findElement(loginButton).click();

        WebElement errorMessageElement = wait.until(ExpectedConditions.visibilityOfElementLocated(errorMessage));

        return errorMessageElement.isDisplayed();
    }

    public Boolean validateRequiredLoginPassword(String userName){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginButton));
        driver.findElement(loginButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(usernameField));
        driver.findElement(usernameField).sendKeys(userName);
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginButton));
        driver.findElement(loginButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(passwordField));
        driver.findElement(passwordField).sendKeys("");
        driver.findElement(loginButton).click();

        WebElement errorMessageElement = wait.until(ExpectedConditions.visibilityOfElementLocated(errorMessage));

        return errorMessageElement.isDisplayed();
    }

    public String getInvalidEmailErrorMessage() {
        WebDriverWait block  = new WebDriverWait(driver, 20);
        WebElement modal = block.until(ExpectedConditions.visibilityOfElementLocated(errorContainer));

        return modal.findElement(errorMessage).getText();
    }

    public String getInvalidPasswordErrorMessage() {
        WebDriverWait block  = new WebDriverWait(driver, 20);
        WebElement modal = block.until(ExpectedConditions.visibilityOfElementLocated(errorContainer));

        return modal.findElement(errorMessage).getText();
    }
}
