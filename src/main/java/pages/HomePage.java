package pages;

import base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends TestBase {
    private WebDriver driver;
    private By userAvatar = By.className("user-avatar");

    public HomePage(WebDriver driver){
        this.driver = driver;
    }

    public Boolean getUserAvatar(){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        WebElement avatar = wait.until(ExpectedConditions.visibilityOfElementLocated(userAvatar));
        return avatar.isDisplayed();
    }
}
